/*
 * wikidata-import-filter
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.swissbib.linked

import org.apache.kafka.streams.KeyValue
import org.apache.kafka.streams.StreamsBuilder
import org.apache.kafka.streams.Topology
import org.apache.logging.log4j.Logger
import org.swissbib.SbMetadataModel
import java.util.*

class KafkaTopology(private val properties: Properties, private val log: Logger) {

    fun build(): Topology {
        val builder = StreamsBuilder()

        val source = builder
            .stream<String, SbMetadataModel>(properties.getProperty("kafka.topic.source"))
            .filter { _, value -> value.data.contains("http://wikiba.se/ontology#Item") }
            .mapValues { value -> value.setData(filterProperties(value.data))}
            .map { _, value -> setKey(value) }
        source.to(properties.getProperty("kafka.topic.sink"))

        return builder.build()
    }

    private fun filterProperties(data: String): String {
        var result = ""
        for (line in data.lines()) {
            if (line.contains("http://www.w3.org/2004/02/skos/core#prefLabel"))
                continue
            else if (line.contains("http://www.w3.org/2000/01/rdf-schema#label"))
                continue
            else
                result += line + "\n"
        }
        return result.trim()
    }

    private val pattern = Regex("<(.*?)>")

    private fun setKey(value: SbMetadataModel): KeyValue<String?, SbMetadataModel> {
        val result = pattern.find(value.data)
        return if (result != null) {
            KeyValue(result.groups[1]?.value, value)
        } else {
            KeyValue(null, value)
        }
    }
}
